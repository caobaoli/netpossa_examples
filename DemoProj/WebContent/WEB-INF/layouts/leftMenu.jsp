<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<ul class="nav nav-list">
	<!-- 用户审核 -->
	<li class="nav-header"><a href="javascript:void(0);"
		data-toggle="collapse" data-target="#ul_lm9801" class="collapsed"><i
			class="icon-lock"></i>用户审核</a> <c:choose>
			<c:when test="${spaceView.tbl2 == '1' or spaceView.tbl2 == '2'}">
				<ul id="ul_lm9801" class="nav nav-list in collapse">
					<li <c:if test="${spaceView.tbl2 == '1'}">class="active"</c:if>>
						<a href="${ctx}/user/audit/userRegistAudit.html?tbl2=1"><i
							class="icon-fire"></i> 用户注册审核</a></li>
					<li <c:if test="${spaceView.tbl2 == '2'}">class="active"</c:if>>
						<a href="${ctx}/user/audit/userEditInfoAudit.html?tbl2=2"><i
							class="icon-fire"></i> 信息修改审核</a></li>
				</ul>
			</c:when>
			<c:otherwise>
				<ul id="ul_lm9801" class="nav nav-list collapse">
					<li <c:if test="${spaceView.tbl2 == '1'}">class="active"</c:if>>
						<a href="${ctx}/user/audit/userRegistAudit.html?tbl2=1"><i
							class="icon-fire"></i> 用户注册审核</a></li>
					<li <c:if test="${spaceView.tbl2 == '2'}">class="active"</c:if>>
						<a href="${ctx}/user/audit/userEditInfoAudit.html?tbl2=2"><i
							class="icon-fire"></i> 信息修改审核</a></li>
				</ul>
			</c:otherwise>

		</c:choose>
	</li>

	<!-- 个人中心 -->
	<li class="nav-header"><a href="javascript:void(0);"
		data-toggle="collapse" data-target="#ul_lm9802"><i
			class="icon-user"></i>个人中心</a> <c:choose>
			<c:when test="${spaceView.tbl2 == '3' or spaceView.tbl2 == '4'}">
				<ul id="ul_lm9802" class="nav nav-list in collapse">
					<li <c:if test="${spaceView.tbl2 == '3'}">class="active"</c:if>>
						<a href="${ctx}/user/center/findUserInfo.html?tbl2=3"><i
							class="icon-fire"></i> 个人信息详情</a></li>
					<li <c:if test="${spaceView.tbl2 == '4'}">class="active"</c:if>>
						<a href="${ctx}/user/center/editUserInfo.html?tbl2=4"><i
							class="icon-fire"></i> 个人信息修改</a></li>
				</ul>
			</c:when>
			<c:otherwise>
				<ul id="ul_lm9802" class="nav nav-list collapse">
					<li <c:if test="${spaceView.tbl2 == '3'}">class="active"</c:if>>
						<a href="${ctx}/user/center/findUserInfo.html?tbl2=3"><i
							class="icon-fire"></i> 个人信息详情</a></li>
					<li <c:if test="${spaceView.tbl2 == '4'}">class="active"</c:if>>
						<a href="${ctx}/user/center/editUserInfo.html?tbl2=4"><i
							class="icon-fire"></i> 个人信息修改</a></li>
				</ul>
			</c:otherwise>

		</c:choose>
	</li>

	<!-- 用户权限管理 -->
	<li class="nav-header"><a href="javascript:void(0);"
		data-toggle="collapse" data-target="#ul_lm9803" class="collapsed"><i
			class="icon-resize-vertical"></i>用户权限管理</a> <c:choose>
			<c:when test="${myPageRequest.tab2 == '5' or myPageRequest.tab2 == '6'}">
				<ul id="ul_lm9803" class="nav nav-list in collapse">
					<li <c:if test="${myPageRequest.tab2 == '5'}">class="active"</c:if>>
						<a href="${ctx}/admin/base/loginSys.html?tab2=5"><i
							class="icon-fire"></i> 用户管理</a></li>
					<li <c:if test="${myPageRequest.tab2 == '6'}">class="active"</c:if>>
						<a href="${ctx}/admin/base/roleManage.html?tab2=6"><i
							class="icon-fire"></i> 角色管理</a></li>
				</ul>
			</c:when>
			<c:otherwise>
				<ul id="ul_lm9803" class="nav nav-list collapse">
					<li <c:if test="${myPageRequest.tab2 == '5'}">class="active"</c:if>>
						<a href="${ctx}/admin/base/loginSys.html?tab2=5"><i
							class="icon-fire"></i> 用户管理</a></li>
					<li <c:if test="${myPageRequest.tab2 == '6'}">class="active"</c:if>>
						<a href="${ctx}/admin/base/roleManage.html?tab2=6"><i
							class="icon-fire"></i> 角色管理</a></li>
				</ul>
			</c:otherwise>

		</c:choose></li>

	<!-- 交易管理 -->
	<li class="nav-header"><a href="javascript:void(0);"
		data-toggle="collapse" data-target="#ul_lm9804" class="collapsed"><i
			class="icon-globe"></i>交易管理</a> <c:choose>
			<c:when test="${spaceView.tbl2 == '7' or spaceView.tbl2 == '8'
				or spaceView.tbl2=='9'}">
				<ul id="ul_lm9804" class="nav nav-list in collapse">
					<li <c:if test="${spaceView.tbl2 == '7'}">class="active"</c:if>>
						<a href="${ctx}/trade/manage/findCurrentTradeList.html?tbl2=7"><i
							class="icon-fire"></i> 即时交易查询</a></li>
					<li <c:if test="${spaceView.tbl2 == '8'}">class="active"</c:if>>
						<a href="${ctx}/trade/manage/findHistoryTradeList.html?tbl2=8"><i
							class="icon-fire"></i> 历史交易查询</a></li>
					<li <c:if test="${spaceView.tbl2 == '9'}">class="active"</c:if>>
						<a href="${ctx}/trade/manage/tradeBillManage.html?tbl2=9"><i
							class="icon-fire"></i> 调账授权管理</a></li>
				</ul>
			</c:when>
			<c:otherwise>
				<ul id="ul_lm9804" class="nav nav-list collapse">
					<li <c:if test="${spaceView.tbl2 == '7'}">class="active"</c:if>>
						<a href="${ctx}/trade/manage/findCurrentTradeList.html?tbl2=7"><i
							class="icon-fire"></i> 即时交易查询</a></li>
					<li <c:if test="${spaceView.tbl2 == '8'}">class="active"</c:if>>
						<a href="${ctx}/trade/manage/findHistoryTradeList.html?tbl2=8"><i
							class="icon-fire"></i> 历史交易查询</a></li>
					<li <c:if test="${spaceView.tbl2 == '9'}">class="active"</c:if>>
						<a href="${ctx}/trade/manage/tradeBillManage.html?tbl2=9"><i
							class="icon-fire"></i> 调账授权管理</a></li>
				</ul>
			</c:otherwise>

		</c:choose>
	</li>

	<!-- 对账管理 -->
	<li class="nav-header"><a href="javascript:void(0);"
		data-toggle="collapse" data-target="#ul_lm9805" class="collapsed"><i
			class="icon-check"></i>对账管理</a> <c:choose>
			<c:when test="${spaceView.tbl2 == '10' or spaceView.tbl2 == '11'}">
				<ul id="ul_lm9805" class="nav nav-list in collapse">
					<li <c:if test="${spaceView.tbl2 == '10'}">class="active"</c:if>>
						<a href="${ctx}/check/bill/dayBill.html?tbl2=10"><i
							class="icon-fire"></i> 日终对账管理</a></li>
					<li <c:if test="${spaceView.tbl2 == '11'}">class="active"</c:if>>
						<a href="${ctx}/check/bill/periodBill.html?tbl2=11"><i
							class="icon-fire"></i> 周期结算管理</a></li>
				</ul>
			</c:when>
			<c:otherwise>
				<ul id="ul_lm9805" class="nav nav-list collapse">
					<li <c:if test="${spaceView.tbl2 == '10'}">class="active"</c:if>>
						<a href="${ctx}/check/bill/dayBill.html?tbl2=10"><i
							class="icon-fire"></i> 日终对账管理</a></li>
					<li <c:if test="${spaceView.tbl2 == '11'}">class="active"</c:if>>
						<a href="${ctx}/check/bill/periodBill.html?tbl2=11"><i
							class="icon-fire"></i> 周期结算管理</a></li>
				</ul>
			</c:otherwise>

		</c:choose>
	</li>

	<!-- 渠道管理 -->
	<li class="nav-header"><a href="javascript:void(0);"
		data-toggle="collapse" data-target="#ul_lm9806" class="collapsed"><i
			class="icon-road"></i>渠道管理</a> <c:choose>
			<c:when test="${spaceView.tbl2 == '13' or spaceView.tbl2 == '14'
				or spaceView.tbl2=='12'}">
				<ul id="ul_lm9806" class="nav nav-list in collapse">
					<li <c:if test="${spaceView.tbl2 == '12'}">class="active"</c:if>>
						<a href="${ctx}/channel/manage/channelManage.html?tbl2=12"><i
							class="icon-fire"></i> 支付渠道管理</a></li>
					<li <c:if test="${spaceView.tbl2 == '13'}">class="active"</c:if>>
						<a href="${ctx}/channel/manage/channelRateManage.html?tbl2=13"><i
							class="icon-fire"></i> 支付渠道费率管理</a></li>
					<li <c:if test="${spaceView.tbl2 == '14'}">class="active"</c:if>>
						<a href="${ctx}/channel/manage/statusManage.html?tbl2=14"><i
							class="icon-fire"></i> 状态码管理</a></li>
				</ul>
			</c:when>
			<c:otherwise>
				<ul id="ul_lm9806" class="nav nav-list collapse">
					<li <c:if test="${spaceView.tbl2 == '12'}">class="active"</c:if>>
						<a href="${ctx}/channel/manage/channelManage.html?tbl2=12"><i
							class="icon-fire"></i> 支付渠道管理</a></li>
					<li <c:if test="${spaceView.tbl2 == '13'}">class="active"</c:if>>
						<a href="${ctx}/channel/manage/channelRateManage.html?tbl2=13"><i
							class="icon-fire"></i> 支付渠道费率管理</a></li>
					<li <c:if test="${spaceView.tbl2 == '14'}">class="active"</c:if>>
						<a href="${ctx}/channel/manage/statusManage.html?tbl2=14"><i
							class="icon-fire"></i> 状态码管理</a></li>
				</ul>
			</c:otherwise>

		</c:choose>
	</li>

	<!-- 退出 -->
	<li class="nav-header"><a href="${ctx}/logout.html"
		data-toggle="collapse" data-target="#ul_lm9807" class="collapsed"><i
			class="icon-off"></i>退出</a>
	</li>

</ul>
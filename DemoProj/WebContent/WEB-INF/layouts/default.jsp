<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title><sitemesh:title /></title>
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
<![endif]-->
<!--[if IE]>
<meta content='IE=7' http-equiv='X-UA-Compatible' />
<![endif]-->
<!--[if lt IE 8]>
<link href="${ctx}/static/styles/ie-8.css" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />

<%@ include file="/components/bootstrap/bootstrap.jsp"%>
<link href="${ctx}/static/styles/quickstart.css" type="text/css" rel="stylesheet" />
<%@ include file="/components/jquery-validation/jquery-validation.jsp" %>
<%@ include file="/components/ztree/zTree.jsp" %>

<script src="${ctx}/static/scripts/system/main.js" type="text/javascript"></script>

<sitemesh:head />
</head>
<body>
	<!-- 此处加载系统头部 -->
	<%@ include file="/WEB-INF/layouts/header.jsp"%>
	<!-- 此处加载系统头部菜单-->
	<%-- <%@ include file="/WEB-INF/layouts/system/topMenu.jsp"%> --%>
	<div class="container-fluid" style="min-height: 554px">
		<div class="row-fluid">
			<div id="left" class="span2 bs-docs-sidebar">
				<!-- 此处加载左侧菜单 -->
                <c:import url="/WEB-INF/layouts/leftMenu.jsp" />
			</div>
			<div id="right" class="span10" style="margin-left: 10px;">
				<!-- 此处加载右侧页面 -->
				<sitemesh:body />
			</div>
		</div>
	</div>
	<!-- 此处加载系统底部 -->
	<%@ include file="/WEB-INF/layouts/footer.jsp"%>
	<!-- 加载遮罩层 -->
	<%@ include file="/WEB-INF/layouts/maskLayer.jsp"%>
	
    <!-- 非启动加载的文件放在下面，使页面更快加载 -->
    <script src="${ctx}/static/scripts/system/common.js" type="text/javascript"></script>
    <%@ include file="/components/bootstrap-datetimepicker/bootstrap-datetimepicker.jsp"%>
    
</body>
</html>
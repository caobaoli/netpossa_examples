<%@page import="java.util.Date"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
	Date date = new Date();
	request.setAttribute("date", date);
%>

<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class='navbar navbar-fixed-top navbar-inverse'>
	<div class='navbar-inner'>
		<div class='container'>
			<a class="brand" href="#">支付中心</a>
			
			<ul class="nav pull-right">
			
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
				    <i class="icon-user"></i> XXXX
				    <span class="caret"></span> 
				</a>
				
				<ul class="dropdown-menu">
					<li><a href="javascript:editProfile();"><i class="icon-pencil"></i>修改个人密码</a></li>
					<li class="divider"></li>
				    <shiro:hasPermission name="btn:user:infoy">
				        <li><a href="#">Y</a></li>
				    </shiro:hasPermission>
				    <shiro:hasPermission name="btn:user:infox">
				        <li><a href="#">X</a></li>
				    </shiro:hasPermission>
				    <shiro:hasPermission name="btn:user:info">
				    </shiro:hasPermission>
					<li><a href="${ctx}/logout.html"><i class="icon-off"></i> 退出</a></li>
				</ul>
				
				<li>
					<a><i class="icon-white icon-time"></i>
				&nbsp;&nbsp;当前日期：<fmt:formatDate value="${date}" pattern="yyyy年MM月dd日"/>
					</a>
				</li>
			</ul>
			
		</div>
	</div>
</div>

<!-- 编辑个人信息对话框 -->
<div class="modal hide fade" style="display: none;" id="profileModal">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>修改个人信息</h3>
  </div>
  <div id="modal_body" class="modal-body">
    <p>对话框内容</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">关闭</a>
    <a href="javascript:submitPwd();" class="btn btn-primary">保存</a>
  </div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script src="<c:url value="/static/scripts/jquery.extend.js"/>" type="text/javascript"></script>
<script src="<c:url value="/static/scripts/PageList.js"/>" type="text/javascript"></script>
<title>角色管理</title>
<script type="text/javascript">
	var pageList = new $.PageList();
	var parameter = {
		// 全选checkbox的id
		selectAllId : 'chkAll',
		// 列表中行选择的checkbox的名称
		rowCheckboxName : 'rowCheckBox',
		// 模块URL
		moduleUrl : '${ctx}/system/role',
		//单个删除还是批量删除都走这个URL
		deletePageUrl : '${ctx}/system/role/remove.do'
	};
	pageList.init(parameter);
	// 单行删除
	function deleteBtn(id) {
		var form = $("#controlForm");
		var confirm = function(v, h, f) {
			if (v == 'ok') {
				form.attr("action", '${ctx}/system/role/delete.do?id=' + id)
						.submit();
				return true;
			} else if (v == 'cancel') {
				return true;
			}
			return true;
		};
		$.jBox.confirm("确定要删除吗？", "提示", confirm, {
			top : '35%'
		});
	}
</script>
</head>
<body>
    
    <p class='breadcrumb'>权限管理 &raquo; 角色列表</p>
    
    <div class='section section-small'>
    
		<div class='section-header'>
			<h5>
				用户列表 <small> <a href="#">${centerUsers.totalElements}</a> </small>
			</h5>
			
		</div>
		
		<div class='section-body'>
			
			<div id="message" class="alert alert-success" <c:if test="${empty message}">style="display: none;"</c:if>>
				<button data-dismiss="alert" class="close">×</button>
				${message}
			</div>
			
			<!-- Search -->
			<form id="controlForm" action="" method="post">
		        <input type="hidden" id="selectedItemIds" name="ids" />
	        </form>
	
	        <form:form class="well form-search" id="searchForm" modelAttribute="roleSearchCondition" action="${ctx}/system/role/list.do" method="post">
		        <form:hidden id="pageNumber" path="pageNumber" />
		        <form:hidden id="pageSize" path="pageSize" />
		        
			        <label for="name" class="control-label" >角色名称：</label> 
			        <form:input path="name" type="text" class="input-medium" id="name" maxlength="12" placeholder="角色名称" />
		
			        <label for="status" class="control-label">角色状态：</label> 
			        <form:input path="status" type="text" class="input-medium" id="status" maxlength="12" placeholder="角色状态" />
		        
			        <input type="submit" class="btn" value="查询" />
		        
	        </form:form>
	        
	        <div class='nav' style="text-align:right;">
		        <input type="button" class="btn btn-primary btn-small" value="新增" onclick="pageList.gotoAddPage();" /> 
		        <input type="button" class="btn btn-primary btn-small" value="删除" onclick="pageList.gotoDeletePage();" />
	        </div>
	        
            <table class="table table-striped table-bordered table-condensed">
		        <thead>
			        <tr>
				        <th class="checkType"><input type="checkbox" name="checkbox" id="chkAll" value="off" onclick="pageList.selectAll()" /></th>
				        <th class="serialnoType">序列</th>
				        <th>角色名称</th>
				        <th>角色编码</th>
				        <th>操作</th>
			        </tr>
		        </thead>
		        <tbody>
			        <c:forEach items="${pageObject.result}" var="record" varStatus="c">
				    <tr>
					    <td class="alignCenter"><input type="checkbox" name="rowCheckBox" id="chk${c.count}" value="${record.id}" /></td>
					    <td class="alignCenter">${c.count}</td>
					    <td class="alignCenter">${record.name}</td>
					    <td class="alignCenter">${record.description}</td>
					    <td class="alignCenter">
						    <div class="toolIcoArea">
							    <a href="show.do?id=${record.id}" title="查看"><i class="icon-info-sign"></i></a> 
							    <a href="edit.do?id=${record.id}" title="编辑"><i class="icon-pencil"></i></a> 
							    <a href="remove.do?id=${record.id}" title="删除"><i class="icon-minus-sign"></i></a>
						    </div>
					    </td>
				    </tr>
			        </c:forEach>
		        </tbody>
	        </table>	        
	        
	        <xhrd:PagingNavigation cssClass="pagination pagination-right"/>
	        
	    </div>
	    
	</div>
	
</body>
</html>
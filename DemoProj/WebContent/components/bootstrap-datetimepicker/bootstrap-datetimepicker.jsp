<%@ page language="java" pageEncoding="UTF-8" %>

<script src="<c:url value="/components/bootstrap-datetimepicker/1.2.0/js/bootstrap-datetimepicker.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/components/bootstrap-datetimepicker/1.2.0/js/locales/bootstrap-datetimepicker.zh-CN.js"/>" charset="UTF-8"></script>
<link  href="<c:url value="/components/bootstrap-datetimepicker/1.2.0/css/datetimepicker.css"/>" rel="stylesheet">
<%@ page language="java" pageEncoding="UTF-8" %>

<script src="<c:url value="/components/jquery/2.0.0/jquery.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/components/jquery-validation/1.9.0/jquery.validate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/components/jquery-validation/1.9.0/messages_bs_cn.js"/>" type="text/javascript"></script>
<script src="<c:url value="/components/jquery-validation/jquery.validate.extend.js"/>" type="text/javascript"></script>
<link  href="<c:url value="/components/jquery-validation/1.9.0/validate.css"/>" type="text/css" rel="stylesheet" />
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s"    uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="xhrd" uri="http://www.xhrd.com/tags/form"%>

<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="sitemesh" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>

<c:set var="ctx" value="${pageContext.request.contextPath}" />

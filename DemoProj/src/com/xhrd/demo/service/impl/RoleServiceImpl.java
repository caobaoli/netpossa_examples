package com.xhrd.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xhrd.demo.dao.RoleDAO;
import com.xhrd.demo.entities.Role;
import com.xhrd.demo.service.RoleService;
import com.xhrd.demo.vo.RoleSearchCondition;
import com.xhrd.framework.vo.Page;

@Service
public class RoleServiceImpl implements RoleService {
    
	@Autowired
    private RoleDAO roleDao;

	@Override
    public Page<Role> searchAndCount(RoleSearchCondition searchCondition) {
		return roleDao.pageQuery("selectPage", searchCondition);
    }


}

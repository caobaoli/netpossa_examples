package com.xhrd.demo.service;

import com.xhrd.demo.entities.Role;
import com.xhrd.demo.vo.RoleSearchCondition;
import com.xhrd.framework.vo.Page;
import com.xhrd.framework.vo.SearchCondition;

public interface RoleService {

	Page<Role> searchAndCount(RoleSearchCondition searchCondition);

}

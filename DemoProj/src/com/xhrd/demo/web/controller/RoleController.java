package com.xhrd.demo.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xhrd.demo.common.util.DateUtil;
import com.xhrd.demo.entities.Role;
import com.xhrd.demo.service.RoleService;
import com.xhrd.demo.vo.RoleSearchCondition;
import com.xhrd.framework.vo.Page;

@Controller
@RequestMapping("/system/role/*")
public class RoleController {
	
	/**
	 * 日志对象
	 */
	protected static Logger logger = LoggerFactory.getLogger(RoleController.class);
	
	@Autowired
	private RoleService roleService;

	/**
	 * 转换页面提交来的日期格式
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(DateUtil.getDateTimePattern()), true));
	}

	@RequestMapping(value = "list.do", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView list(RoleSearchCondition searchCondition, BindingResult bindingResult) {
		logger.debug("beging process list.do");
		String viewName = "/demo/role/list";
		ModelAndView mav = new ModelAndView(viewName);
		Page<Role> page = roleService.searchAndCount(searchCondition);
		mav.addObject("pageObject", page);
		logger.debug("end process list.do");
		return mav;
	}

}

package com.xhrd.demo.vo;

import com.xhrd.framework.vo.SearchCondition;

public class RoleSearchCondition extends SearchCondition {

	/**
	 * serialUID
	 */
	private static final long serialVersionUID = 6043454446886384578L;
	private String name;
	private String description;
	private String status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}

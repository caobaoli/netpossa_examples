package com.xhrd.demo.dao.mapper;

import com.xhrd.demo.entities.Role;
import com.xhrd.demo.vo.RoleSearchCondition;
import com.xhrd.framework.dao.BaseInterface;

public interface RoleMapper extends BaseInterface<Role> {

	int selectPageCount(RoleSearchCondition searchCondition);

}

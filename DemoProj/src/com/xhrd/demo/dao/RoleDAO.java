package com.xhrd.demo.dao;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xhrd.demo.dao.mapper.RoleMapper;
import com.xhrd.demo.entities.Role;
import com.xhrd.demo.vo.RoleSearchCondition;
import com.xhrd.framework.vo.Page;

@Repository
public class RoleDAO {

	@Autowired
	private RoleMapper roleMapper;

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public Page<Role> pageQuery(String string, RoleSearchCondition searchCondition) {

		Number totalCount = (Number) roleMapper.selectPageCount(searchCondition);
		if (totalCount == null || totalCount.longValue() <= 0) {
			return new Page<Role>(searchCondition, 0);
		}

		Page<Role> page = new Page<Role>(searchCondition, totalCount.intValue());
		RowBounds bounds = new RowBounds(page.getFirstResult(), page.getPageSize());
		List<Role> list = sqlSessionTemplate.selectList("com.xhrd.demo.dao.mapper.RoleMapper.selectPage", searchCondition, bounds);
		page.setResult(list);

		return page;
	}

}
